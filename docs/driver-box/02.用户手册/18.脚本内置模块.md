---
title: 脚本内置模块
date: 2023-04-23 14:40:22
permalink: /pages/28a924/
---

# 脚本内置模块

为了扩展脚本功能性，我们默认为 lua 脚本提供一些开箱即用的一些模块。

## 内置模块

### JSON

json 模块：使用户可以在 lua 脚本中对数据快速进行序列化、反序列化。

**方法**

* json.encode(obj)
* json.decode(string)

**示例**

```lua
local json = requre("json")

function JSONEncode()
	local data = { name = "tom" }
	return json.encode(data)
end

function JSONDecode()
	local s = '{"name":"tom"}'
	return json.decode(s)
end
```

### HTTP

http 模块：使用户可以在 lua 脚本中发起各种请求，为 lua 脚本提供一个 http 客户端，方便一些业务处理。

**方法**

* http.delete(url [, options])
* http.get(url [, options])
* http.head(url [, options])
* http.patch(url [, options])
* http.post(url [, options])
* http.put(url [, options])
* http.request(method, url [, options])
* http.request_batch(requests)
* http.response

**Options**

| Name    | Type   | Description |
| ------- | ------ | ----------- |
| query   | String | URL编码的查询参数 |
| cookies | Table  | 随请求发送的附加cookies |
| body    | String | 请求正文 |
| headers | Table  | 随请求发送的附加标头 |
| timeout | Number/String | 请求超时。秒数或字符串，如“1h” |
| auth    | Table  | HTTP基本身份验证的用户名和密码。表键是*user*表示用户名，*pass*表示密码`auth=｛user=“user”，pass=“pass”｝` |

**Response**

| Name        | Type   | Description |
| ----------- | ------ | ----------- |
| body        | String | 响应内容 |
| body_size   | Number | 响应内容大小 |
| headers     | Table  | 响应头 |
| cookies     | Table  | cookie |
| status_code | Number | 状态码 |
| url         | String | 重定向后请求结束指向的最终URL |

**示例**

```lua
local http = require("http")

function request()
	local res, err_message = http.get("http://www.baidu.com", {
		timeout = "5s"
	})
	print(res, err_message)
end
```

### DriverBox

DriverBox 模块：使用户可以在 lua 脚本中调用我们开放的API，从而满足一些复杂的业务场景。

**方法**

* driverbox.getCache(string)
* driverbox.setCache(string, string)
* driverbox.writeToMsgBus(deviceName, values)

**示例**

```lua
local app = require("driverbox")

-- 点位数据写入消息总线
function report( ... )
	local values = {
		{
			name = "onOff",
			value = "1",
		},
		{
			name = "co2",
			value = "20",
		}
	}
	app.writeToMsgBus("sensor_1", values)
end
```
