---
home: true

heroText: iBUILDING
tagline: Make iBUILDING come TRUE

[//]: # (bannerBg: none # auto => 网格纹背景&#40;有bodyBgImg时无背景&#41;，默认 | none => 无 | '大图地址' | background: 自定义背景样式       提示：如发现文本颜色不适应你的背景时可以到palette.styl修改$bannerTextColor变量)

# 文章列表显示方式: detailed 默认，显示详细版文章列表（包括作者、分类、标签、摘要、分页等）| simple => 显示简约版文章列表（仅标题和日期）| none 不显示文章列表
postList: none
---

![](/img.png)

## 🎖明星项目
::: cardList 3
```yaml
- name:  🔥 driver-box
  desc: 驱动编排引擎
  link: /driver-box
  bgColor: '#3a9bb0'
  textColor: '#ffffff'
```
:::

<!-- AD -->

[//]: # (<div class="wwads-cn wwads-horizontal page-wwads" data-id="136"></div>)
<style>
  .page-wwads{
    width:100%!important;
    min-height: 0;
    margin: 0;
  }
  .page-wwads .wwads-img img{
    width:80px!important;
  }
  .page-wwads .wwads-poweredby{
    width: 40px;
    position: absolute;
    right: 25px;
    bottom: 3px;
  }
  .wwads-content .wwads-text, .page-wwads .wwads-text{
    height: 100%;
    padding-top: 5px;
    display: block;
  }
</style>
